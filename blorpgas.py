from irc.bot import SingleServerIRCBot
from libs.chatterbotapi import ChatterBotFactory, ChatterBotType
from libs.Hangman import Hangman
import re


server = u"irc.synirc.com"
channel = u"#rainwave"
#channel = u"#rainwave"
nickname = u"blorpgas"

ALLOW_CHAT = 0
ALLOW_HANGMAN = 1


class blorpgas(SingleServerIRCBot):

	
	
	def __init__(self):
		SingleServerIRCBot.__init__(self, [(server, 6667)], nickname, nickname)
		
		if ALLOW_CHAT:
			factory = ChatterBotFactory()
			self.bot = factory.create(ChatterBotType.CLEVERBOT)
			self.bot_session = self.bot.create_session()
		if ALLOW_HANGMAN:
			self.hangman=Hangman()
			self.hangman.new_game()
	
	def _to_irc(self, c, msgtype, target, msg):
		'''Send an IRC message'''

		if hasattr(c, msgtype):
			f = getattr(c, msgtype)
			if type(msg) is not unicode:
				msg = unicode(msg, u'utf-8')
			try:
				f(target, msg)
			except:
				print "Sending error!"

	
	def on_welcome ( self, c, e ):
		c.join(channel)
		print "Connection complete."
	
	def on_pubmsg(self, c, e):
		nick = e.source.nick
		msg = e.arguments[0].strip()
		chan = e.target
		print "Got message : {}".format(msg)
		if ALLOW_HANGMAN and re.match("^%hm ", msg):
			cmd = msg.replace("%hm ", "").strip()
			print "Hangman command : {}".format(cmd)
			if cmd == "new":
				self._to_irc(c, u'privmsg', channel, "New game : {}".format(self.hangman.new_game()))
			elif cmd == "display":
				self._to_irc(c, u'privmsg', channel, self.hangman.display_stats())
			elif re.match('^[a-zA-Z0-9]{1}$', cmd):
				resp = self.hangman.guess(cmd)
				if resp:
					self._to_irc(c, u'privmsg', channel, "{} | {}".format(resp, self.hangman.display_stats()))
				else:
					self._to_irc(c, u'privmsg', channel, self.hangman.display_stats())
			else:
				self._to_irc(c, u'privmsg', channel, "Available commands : new, display or type any letter or number to guess.")
				
		if ALLOW_CHAT and nickname in msg:
			if msg.index(nickname):
				msg = msg.replace(nickname, "")
			reply = self.bot_session.think(msg.replace(nickname, "").strip())
			self._to_irc(c, u'privmsg', channel, u"{}: {}".format(nick, reply))
			
def main():
	bot = blorpgas()
	bot.start()

if __name__ == "__main__":
	main()
