import random
import re

word_dict = "libs/hangman_dict.txt"

MAX_MISS = 10

class Hangman():

	def __init__(self):
		self.word_to_guess=""
		self.word_with_blank=""
		self.letters_guessed = []
		self.miss = 0
		
	def new_game(self):
		with open(word_dict, "r") as f:
    			content = f.readlines()
		self.word_to_guess = random.choice(content)
		self.word_with_blank = re.sub('[a-zA-Z0-9]', '_', self.word_to_guess)
		self.letters_guessed = []
		self.miss = 0
		return "{}".format(self.word_with_blank)
		
	def guess(self, letter):
		indexes_in_word = []
		if re.match('^[a-zA-Z0-9]{1}$', letter):
			letter=letter.lower()
			if letter not in self.letters_guessed:
				self.letters_guessed.append(letter)
				if letter in self.word_to_guess.lower():
					i=0
					self.word_with_blank=list(self.word_with_blank)
					for l in self.word_to_guess.lower():
						if letter==l:
							self.word_with_blank[i] = self.word_to_guess[i]
						i=i+1					
					self.word_with_blank="".join(self.word_with_blank)
					
					if "_" not in self.word_with_blank:
						game=self.word_to_guess
						self.new_game()
						return "You won! The game was {}. Starting new game!".format(game.strip())
					return None
				else:
					self.miss = self.miss+1
					if self.miss >= MAX_MISS:
						lost_word = self.word_to_guess
						self.new_game()
						return "You lost! The word was : {}".format(lost_word)
					return "Character not in word."
			else:
				return "Already tried this character."
		else:
			return "Not a valid character."
	
	def display_stats(self):
		return "Word : {} | Miss(es):{} (Max:{}) | Characters used : {}".format(
			self.word_with_blank.replace("", " ").strip(),
			self.miss,
			MAX_MISS,
			", ".join(self.letters_guessed)
		)

if __name__ == "__main__":
	hm=Hangman()
	hm.new_game()
	
	print hm.word_to_guess
	
	while(True):
		ri=raw_input("Guess letter : ")
		if ri == "exit":
			break;
		if ri == "new":
			hm.new_game()
			print hm.word_to_guess
		else:
			ret=hm.guess(ri)
			if ret:
				print ret
		print hm.display_stats()
